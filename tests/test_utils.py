# Pytest imports
import pytest

# First party imports
from exochoices.exceptions import UnknownError, get_error


def test_unknwon_error():
    with pytest.raises(UnknownError):
        get_error("TOTO", "toto", ["toto"])
