# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exochoices import PublicationType as pT
from exochoices.exceptions import MandatoryFieldError, UnknownPublicationTypeError


@pytest.mark.parametrize(
    "choice, expected_int, expected_str",
    [
        param(pT.BOOK, 1, "book", id="book"),
        param(pT.THESIS, 2, "thesis", id="thesis"),
        param(pT.REFEREED_ARTICLE, 3, "refereed article", id="refereed article"),
        param(pT.PROCEEDING, 4, "proceeding", id="proceeding"),
        param(pT.UNKNOWN, 5, "unknown", id="unknown"),
        param(pT.REPORT, 6, "report", id="report"),
        param(pT.ARXIV, 7, "arXiv", id="arxiv"),
    ],
)
def test_publicaton_type(choice, expected_int, expected_str):
    with soft_assertions():
        assert_that(choice.as_int).is_equal_to(expected_int)
        assert_that(choice.as_str).is_equal_to(expected_str)


def test_as_str_list_publication():
    expected = [
        "book",
        "thesis",
        "refereed article",
        "proceeding",
        "unknown",
        "report",
        "arXiv",
    ]
    assert_that(pT.as_str_list()).is_equal_to(expected)


def test_as_doc_publication():
    expected = [
        ("book", 1),
        ("thesis", 2),
        ("refereed article", 3),
        ("proceeding", 4),
        ("unknown", 5),
        ("report", 6),
        ("arXiv", 7),
    ]
    assert_that(pT.as_doc()).is_equal_to(expected)


@pytest.mark.parametrize(
    "choice, expected",
    [
        (pT.BOOK, 1),
        (pT.THESIS, 2),
        (pT.REFEREED_ARTICLE, 3),
        (pT.PROCEEDING, 4),
        (pT.UNKNOWN, 5),
        (pT.REPORT, 6),
        (pT.ARXIV, 7),
    ],
)
def test_as_decimal_publication(choice, expected):
    assert_that(choice.as_decimal).is_equal_to(Decimal(expected))


def test_from_str_publication():
    strings = pT.as_str_list()
    expected = list(pT)

    with soft_assertions():
        for string, expect in zip(strings, expected):
            assert_that(pT.from_str(string)).is_equal_to(expect)


def test_case_from_str_publication():
    assert_that(pT.from_str("proCEEDiNG")).is_equal_to(pT.PROCEEDING)


@pytest.mark.parametrize(
    "enum, param, exception",
    [
        # Mandatory
        param(pT, None, MandatoryFieldError, id="Publication type with None"),
        # Not str
        param(pT, 1, TypeError, id="Publication type with int"),
        # Unknown
        param(
            pT,
            "toto",
            UnknownPublicationTypeError,
            id="Publication type with unknown",
        ),
    ],
)
def test_exception_from_str_publication(enum, param, exception):
    with pytest.raises(exception):
        enum.from_str(param)
