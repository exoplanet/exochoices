# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exochoices import MassUnit as mU
from exochoices import RadiusUnit as rU
from exochoices.exceptions import MandatoryFieldError, UnknownUnitError


@pytest.mark.parametrize(
    "choice, expected_int, expected_str",
    [
        # Mass Units
        param(mU.MJUP, 1, "Mjup", id="jupiter mass"),
        param(mU.MEARTH, 2, "Mearth", id="earth mass"),
        # Radius Units
        param(rU.RJUP, 1, "Rjup", id="jupiter radius"),
        param(rU.REARTH, 2, "Rearth", id="earth radius"),
    ],
)
def test_planet_and_web_statuses(choice, expected_int, expected_str):
    with soft_assertions():
        assert_that(choice.as_int).is_equal_to(expected_int)
        assert_that(choice.as_str).is_equal_to(expected_str)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            mU,
            [
                "Mjup",
                "Mearth",
            ],
            id="mass unit",
        ),
        param(
            rU,
            [
                "Rjup",
                "Rearth",
            ],
            id="radius unit",
        ),
    ],
)
def test_as_str_list_unit(enum, expected):
    assert_that(enum.as_str_list()).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            mU,
            [
                ("Mjup", 1),
                ("Mearth", 2),
            ],
            id="mass unit",
        ),
        param(
            rU,
            [
                ("Rjup", 1),
                ("Rearth", 2),
            ],
            id="radius unit",
        ),
    ],
)
def test_as_doc_unit(enum, expected):
    assert_that(enum.as_doc()).is_equal_to(expected)


@pytest.mark.parametrize(
    "choice, expected",
    [
        # Mass Units
        (mU.MJUP, 1),
        (mU.MEARTH, 2),
        # Radius Units
        (rU.RJUP, 1),
        (rU.REARTH, 2),
    ],
)
def test_as_decimal_unit(choice, expected):
    assert_that(choice.as_decimal).is_equal_to(Decimal(expected))


@pytest.mark.parametrize(
    "enum, strings, expected",
    [
        param(mU, mU.as_str_list(), list(mU), id="Mass unit"),
        param(rU, rU.as_str_list(), list(rU), id="Radius unit"),
    ],
)
def test_from_str_unit(enum, strings, expected):
    with soft_assertions():
        for string, expect in zip(strings, expected):
            assert_that(enum.from_str(string)).is_equal_to(expect)


@pytest.mark.parametrize(
    "enum, string, expected",
    [
        param(mU, "mjuP", mU.MJUP, id="Mass unit case"),
        param(rU, "REARTH", rU.REARTH, id="Radius unit case"),
    ],
)
def test_case_from_str_unit(enum, string, expected):
    assert_that(enum.from_str(string)).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, param, exception",
    [
        # Mandatory
        param(mU, None, MandatoryFieldError, id="Mass unit with None"),
        param(rU, None, MandatoryFieldError, id="Radius unit with None"),
        # Not str
        param(mU, 1, TypeError, id="Mass unit with int"),
        param(rU, 1, TypeError, id="Radius unit with int"),
        # Unknown
        param(mU, "toto", UnknownUnitError, id="Mass unit with unknown"),
        param(rU, "toto", UnknownUnitError, id="Radius unit with unknown"),
    ],
)
def test_exception_from_str_unit(enum, param, exception):
    with pytest.raises(exception):
        enum.from_str(param)
