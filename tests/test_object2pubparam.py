# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exochoices import Plan2PubParam as p2PP
from exochoices import Star2PubParam as s2PP
from exochoices.exceptions import MandatoryFieldError, UnknownPubParamError


@pytest.mark.parametrize(
    "choice, expected_int, expected_str",
    [
        # Star to Publication Parameter
        param(s2PP.GENERIC_PUB, 0, "<generic_pub>", id="generic publication; star"),
        param(s2PP.DISTANCE, 1, "distance", id="distance; star"),
        param(s2PP.MASS, 2, "mass", id="mass; star"),
        param(s2PP.AGE, 3, "age", id="age; star"),
        param(s2PP.TEFF, 4, "teff", id="teff; star"),
        param(s2PP.RADIUS, 5, "radius", id="radius; star"),
        param(s2PP.METALLICITY, 6, "metallicity", id="metallicity; star"),
        param(s2PP.MAGNETIC_FIELD, 7, "magnetic_field", id="magnetic field; star"),
        param(s2PP.DETECTED_DISC, 8, "detected_disc", id="detected disc; star"),
        # Planet to Publication Parameter
        param(p2PP.GENERIC_PUB, 0, "<generic_pub>", id="generic publication; planet"),
        param(p2PP.MASS, 1, "mass", id="mass; planet"),
        param(p2PP.RADIUS, 2, "radius", id="radius; planet"),
        param(p2PP.AXIS, 3, "axis", id="axis; planet"),
        param(p2PP.PERIOD, 4, "period", id="period; planet"),
        param(p2PP.ECCENTRICITY, 5, "eccentricity", id="eccentricity; planet"),
        param(p2PP.OMEGA, 6, "omega", id="omega; planet"),
        param(p2PP.INCLINATION, 7, "inclination", id="inclination; planet"),
        param(p2PP.TZERO_TR, 8, "tzero_tr", id="tzero tr; planet"),
        param(p2PP.TZERO_VR, 9, "tzero_vr", id="tzero vr; planet"),
        param(p2PP.TPERI, 10, "tperi", id="tperi; planet"),
        param(p2PP.TCONJ, 11, "tconj", id="tconj; planet"),
        param(
            p2PP.TEMP_CALCULATED, 12, "temp_calculated", id="temp calculated; planet"
        ),
        param(p2PP.TEMP_MEASURED, 13, "temp_measured", id="temp measured; planet"),
        param(p2PP.HOT_POINT_LON, 14, "hot_point_lon", id="hot point lon; planet"),
        param(p2PP.LOG_G, 15, "log_g", id="log g; planet"),
        param(p2PP.TZERO_TR_SEC, 16, "tzero_tr_sec", id="tzero tr sec; planet"),
        param(p2PP.LAMBDA_ANGLE, 17, "lambda_angle", id="lamba angle; planet"),
        param(p2PP.K, 18, "k", id="k; planet"),
        param(
            p2PP.GEOMETRIC_ALBEDO, 19, "geometric_albedo", id="geometric albedo; planet"
        ),
        param(
            p2PP.IMPACT_PARAMETER, 20, "impact_parameter", id="impact parameter; planet"
        ),
        param(p2PP.MASS_SINI, 21, "mass_sini", id="mass sin i; planet"),
    ],
)
def test_2_pub_parameter(choice, expected_int, expected_str):
    with soft_assertions():
        assert_that(choice.as_int).is_equal_to(expected_int)
        assert_that(choice.as_str).is_equal_to(expected_str)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            s2PP,
            [
                "<generic_pub>",
                "distance",
                "mass",
                "age",
                "teff",
                "radius",
                "metallicity",
                "magnetic_field",
                "detected_disc",
            ],
            id="star to publication parameter",
        ),
        param(
            p2PP,
            [
                "<generic_pub>",
                "mass",
                "radius",
                "axis",
                "period",
                "eccentricity",
                "omega",
                "inclination",
                "tzero_tr",
                "tzero_vr",
                "tperi",
                "tconj",
                "temp_calculated",
                "temp_measured",
                "hot_point_lon",
                "log_g",
                "tzero_tr_sec",
                "lambda_angle",
                "k",
                "geometric_albedo",
                "impact_parameter",
                "mass_sini",
            ],
            id="planet to publication parameter",
        ),
    ],
)
def test_as_str_list_2_pub_parameter(enum, expected):
    assert_that(enum.as_str_list()).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            s2PP,
            [
                ("<generic_pub>", 0),
                ("distance", 1),
                ("mass", 2),
                ("age", 3),
                ("teff", 4),
                ("radius", 5),
                ("metallicity", 6),
                ("magnetic_field", 7),
                ("detected_disc", 8),
            ],
            id="star to publication parameter",
        ),
        param(
            p2PP,
            [
                ("<generic_pub>", 0),
                ("mass", 1),
                ("radius", 2),
                ("axis", 3),
                ("period", 4),
                ("eccentricity", 5),
                ("omega", 6),
                ("inclination", 7),
                ("tzero_tr", 8),
                ("tzero_vr", 9),
                ("tperi", 10),
                ("tconj", 11),
                ("temp_calculated", 12),
                ("temp_measured", 13),
                ("hot_point_lon", 14),
                ("log_g", 15),
                ("tzero_tr_sec", 16),
                ("lambda_angle", 17),
                ("k", 18),
                ("geometric_albedo", 19),
                ("impact_parameter", 20),
                ("mass_sini", 21),
            ],
            id="planet to publication parameter",
        ),
    ],
)
def test_as_doc_2_pub_parameter(enum, expected):
    assert_that(enum.as_doc()).is_equal_to(expected)


@pytest.mark.parametrize(
    "choice, expected",
    [
        # Star to Publication Parameter
        (s2PP.GENERIC_PUB, 0),
        (s2PP.DISTANCE, 1),
        (s2PP.MASS, 2),
        (s2PP.AGE, 3),
        (s2PP.TEFF, 4),
        (s2PP.RADIUS, 5),
        (s2PP.METALLICITY, 6),
        (s2PP.MAGNETIC_FIELD, 7),
        (s2PP.DETECTED_DISC, 8),
        # Planet to Publication Parameter
        (p2PP.GENERIC_PUB, 0),
        (p2PP.MASS, 1),
        (p2PP.RADIUS, 2),
        (p2PP.AXIS, 3),
        (p2PP.PERIOD, 4),
        (p2PP.ECCENTRICITY, 5),
        (p2PP.OMEGA, 6),
        (p2PP.INCLINATION, 7),
        (p2PP.TZERO_TR, 8),
        (p2PP.TZERO_VR, 9),
        (p2PP.TPERI, 10),
        (p2PP.TCONJ, 11),
        (p2PP.TEMP_CALCULATED, 12),
        (p2PP.TEMP_MEASURED, 13),
        (p2PP.HOT_POINT_LON, 14),
        (p2PP.LOG_G, 15),
        (p2PP.TZERO_TR_SEC, 16),
        (p2PP.LAMBDA_ANGLE, 17),
        (p2PP.K, 18),
        (p2PP.GEOMETRIC_ALBEDO, 19),
        (p2PP.IMPACT_PARAMETER, 20),
        (p2PP.MASS_SINI, 21),
    ],
)
def test_as_decimal_2_pub_param(choice, expected):
    assert_that(choice.as_decimal).is_equal_to(Decimal(expected))


@pytest.mark.parametrize(
    "enum, strings, expected",
    [
        param(s2PP, s2PP.as_str_list(), list(s2PP), id="Star to publication param"),
        param(p2PP, p2PP.as_str_list(), list(p2PP), id="Planet to publication param"),
    ],
)
def test_from_str_2_pub_param(enum, strings, expected):
    with soft_assertions():
        for string, expect in zip(strings, expected):
            assert_that(enum.from_str(string)).is_equal_to(expect)


@pytest.mark.parametrize(
    "enum, string, expected",
    [
        param(s2PP, "DisTANce", s2PP.DISTANCE, id="Star to pub param case"),
        param(p2PP, "LoG_G", p2PP.LOG_G, id="Planet to pub param case"),
    ],
)
def test_case_from_str_2_pub_param(enum, string, expected):
    assert_that(enum.from_str(string)).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, param, exception",
    [
        # Mandatory
        param(s2PP, None, MandatoryFieldError, id="Star2PubParam with None"),
        param(p2PP, None, MandatoryFieldError, id="Planet2PubParam with None"),
        # Not str
        param(s2PP, 1, TypeError, id="Star2PubParam with int"),
        param(p2PP, 1, TypeError, id="Planet2PubParam with int"),
        # Unknown
        param(s2PP, "toto", UnknownPubParamError, id="Star2PubParam with unknown"),
        param(p2PP, "toto", UnknownPubParamError, id="Planet2PubParam with unknown"),
    ],
)
def test_exception_from_str_2_pub_param(enum, param, exception):
    with pytest.raises(exception):
        enum.from_str(param)
