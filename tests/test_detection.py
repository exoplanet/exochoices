# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exochoices import DetectedDiscDetection as ddD
from exochoices import MassDetection as mD
from exochoices import PlanetDetection as pD
from exochoices import RadiusDetection as rD
from exochoices.detection import _AllDetection as aD
from exochoices.exceptions import MandatoryFieldError, UnknownDetectionTypeError


@pytest.mark.parametrize(
    "choice, expected_int, expected_str",
    [
        # All Detection
        param(aD.RADIAL_VELOCITY, 1, "Radial Velocity", id="radial velocity; all"),
        param(aD.TIMING, 2, "Timing", id="timing; all"),
        param(aD.CONTROVERSIAL, 3, "Controversial", id="controversial; all"),
        param(aD.MICROLENSING, 4, "Microlensing", id="microlensing; all"),
        param(aD.IMAGING, 5, "Imaging", id="imaging; all"),
        param(aD.PRIMARY_TRANSIT, 6, "Primary Transit", id="primary transit; all"),
        param(aD.ASTROMETRY, 7, "Astrometry", id="astrometry; all"),
        param(aD.TTV, 8, "TTV", id="ttv; all"),
        param(aD.OTHER, 9, "Other", id="other; all"),
        param(aD.SPECTRUM, 10, "Spectrum", id="spectrum; all"),
        param(aD.THEORETICAL, 11, "Theoretical", id="theoretical; all"),
        param(aD.FLUX, 12, "Flux", id="flux; all"),
        param(
            aD.SECONDARY_TRANSIT,
            13,
            "Secondary Transit",
            id="secondary transit; all",
        ),
        param(aD.IR_EXCESS, 14, "IR Excess", id="ir excess; all"),
        # Planet Detection
        param(pD.RADIAL_VELOCITY, 1, "Radial Velocity", id="radial velocity; planet"),
        param(pD.TIMING, 2, "Timing", id="timing; planet"),
        param(pD.MICROLENSING, 4, "Microlensing", id="microlensing; planet"),
        param(pD.IMAGING, 5, "Imaging", id="imaging; planet"),
        param(pD.PRIMARY_TRANSIT, 6, "Primary Transit", id="primary transit; planet"),
        param(pD.ASTROMETRY, 7, "Astrometry", id="astrometry; planet"),
        param(pD.TTV, 8, "TTV", id="ttv; planet"),
        param(pD.OTHER, 9, "Other", id="other; planet"),
        param(
            pD.SECONDARY_TRANSIT,
            13,
            "Secondary Transit",
            id="secondary transit; planet",
        ),
        # Mass Detection
        param(mD.RADIAL_VELOCITY, 1, "Radial Velocity", id="radial velocity; mass"),
        param(mD.TIMING, 2, "Timing", id="timing; mass"),
        param(mD.CONTROVERSIAL, 3, "Controversial", id="controversial; mass"),
        param(mD.MICROLENSING, 4, "Microlensing", id="microlensing; mass"),
        param(mD.ASTROMETRY, 7, "Astrometry", id="astrometry; mass"),
        param(mD.TTV, 8, "TTV", id="ttv; mass"),
        param(mD.SPECTRUM, 10, "Spectrum", id="spectrum; mass"),
        param(mD.THEORETICAL, 11, "Theoretical", id="theoretical; mass"),
        # Radius Detection
        param(rD.PRIMARY_TRANSIT, 6, "Primary Transit", id="primary transit; radius"),
        param(rD.THEORETICAL, 11, "Theoretical", id="theoretical; radius"),
        param(rD.FLUX, 12, "Flux", id="flux; radius"),
        # Detected Disc Detection
        param(ddD.IMAGING, 5, "Imaging", id="imaging; detected disc"),
        param(ddD.IR_EXCESS, 14, "IR Excess", id="ir excess; detected disc"),
    ],
)
def test_detection(choice, expected_int, expected_str):
    with soft_assertions():
        assert_that(choice.as_int).is_equal_to(expected_int)
        assert_that(choice.as_str).is_equal_to(expected_str)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            aD,
            [
                "Radial Velocity",
                "Timing",
                "Controversial",
                "Microlensing",
                "Imaging",
                "Primary Transit",
                "Astrometry",
                "TTV",
                "Other",
                "Spectrum",
                "Theoretical",
                "Flux",
                "Secondary Transit",
                "IR Excess",
            ],
            id="All detection",
        ),
        param(
            pD,
            [
                "Radial Velocity",
                "Timing",
                "Microlensing",
                "Imaging",
                "Primary Transit",
                "Astrometry",
                "TTV",
                "Other",
                "Secondary Transit",
            ],
            id="planet detection",
        ),
        param(
            mD,
            [
                "Radial Velocity",
                "Timing",
                "Controversial",
                "Microlensing",
                "Astrometry",
                "TTV",
                "Spectrum",
                "Theoretical",
            ],
            id="mass detection",
        ),
        param(
            rD,
            [
                "Primary Transit",
                "Theoretical",
                "Flux",
            ],
            id="radius detection",
        ),
        param(
            ddD,
            [
                "Imaging",
                "IR Excess",
            ],
            id="detected disc detection",
        ),
    ],
)
def test_as_str_list_detection(enum, expected):
    assert_that(enum.as_str_list()).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            aD,
            [
                ("Radial Velocity", 1),
                ("Timing", 2),
                ("Controversial", 3),
                ("Microlensing", 4),
                ("Imaging", 5),
                ("Primary Transit", 6),
                ("Astrometry", 7),
                ("TTV", 8),
                ("Other", 9),
                ("Spectrum", 10),
                ("Theoretical", 11),
                ("Flux", 12),
                ("Secondary Transit", 13),
                ("IR Excess", 14),
            ],
            id="All detection",
        ),
        param(
            pD,
            [
                ("Radial Velocity", 1),
                ("Timing", 2),
                ("Microlensing", 4),
                ("Imaging", 5),
                ("Primary Transit", 6),
                ("Astrometry", 7),
                ("TTV", 8),
                ("Other", 9),
                ("Secondary Transit", 13),
            ],
            id="planet detection",
        ),
        param(
            mD,
            [
                ("Radial Velocity", 1),
                ("Timing", 2),
                ("Controversial", 3),
                ("Microlensing", 4),
                ("Astrometry", 7),
                ("TTV", 8),
                ("Spectrum", 10),
                ("Theoretical", 11),
            ],
            id="mass detection",
        ),
        param(
            rD,
            [
                ("Primary Transit", 6),
                ("Theoretical", 11),
                ("Flux", 12),
            ],
            id="radius detection",
        ),
        param(
            ddD,
            [
                ("Imaging", 5),
                ("IR Excess", 14),
            ],
            id="detected disc detection",
        ),
    ],
)
def test_as_doc_detection(enum, expected):
    assert_that(enum.as_doc()).is_equal_to(expected)


@pytest.mark.parametrize(
    "choice, expected",
    [
        # All detection
        (aD.RADIAL_VELOCITY, 1),
        (aD.TIMING, 2),
        (aD.CONTROVERSIAL, 3),
        (aD.MICROLENSING, 4),
        (aD.IMAGING, 5),
        (aD.PRIMARY_TRANSIT, 6),
        (aD.ASTROMETRY, 7),
        (aD.TTV, 8),
        (aD.OTHER, 9),
        (aD.SPECTRUM, 10),
        (aD.THEORETICAL, 11),
        (aD.FLUX, 12),
        (aD.SECONDARY_TRANSIT, 13),
        (aD.IR_EXCESS, 14),
        # Planet Detection
        (pD.RADIAL_VELOCITY, 1),
        (pD.TIMING, 2),
        (pD.MICROLENSING, 4),
        (pD.IMAGING, 5),
        (pD.PRIMARY_TRANSIT, 6),
        (pD.ASTROMETRY, 7),
        (pD.TTV, 8),
        (pD.OTHER, 9),
        (pD.SECONDARY_TRANSIT, 13),
        # Mass Detection
        (mD.RADIAL_VELOCITY, 1),
        (mD.TIMING, 2),
        (mD.CONTROVERSIAL, 3),
        (mD.MICROLENSING, 4),
        (mD.ASTROMETRY, 7),
        (mD.TTV, 8),
        (mD.SPECTRUM, 10),
        (mD.THEORETICAL, 11),
        # Radius Detection
        (rD.PRIMARY_TRANSIT, 6),
        (rD.THEORETICAL, 11),
        (rD.FLUX, 12),
        # Detected Disc Detection
        (ddD.IMAGING, 5),
        (ddD.IR_EXCESS, 14),
    ],
)
def test_as_decimal_detection(choice, expected):
    assert_that(choice.as_decimal).is_equal_to(Decimal(expected))


@pytest.mark.parametrize(
    "enum, strings, expected",
    [
        param(aD, aD.as_str_list(), list(aD), id="All detection"),
        param(pD, pD.as_str_list(), list(pD), id="Planet Detection"),
        param(mD, mD.as_str_list(), list(mD), id="Mass detection"),
        param(rD, rD.as_str_list(), list(rD), id="Radius detection"),
        param(ddD, ddD.as_str_list(), list(ddD), id="Detected Disc detection"),
    ],
)
def test_from_str_detection(enum, strings, expected):
    with soft_assertions():
        for string, expect in zip(strings, expected):
            assert_that(enum.from_str(string)).is_equal_to(expect)


@pytest.mark.parametrize(
    "enum, string, expected",
    [
        param(aD, "radial VELOcity", aD.RADIAL_VELOCITY, id="All detection case"),
        param(pD, "TIMIng", pD.TIMING, id="Planet detection case"),
        param(mD, "miCROlensING", mD.MICROLENSING, id="Mass detection case"),
        param(rD, "FlUx", rD.FLUX, id="Radius detection case"),
        param(ddD, "Ir ExcESs", ddD.IR_EXCESS, id="Detected disc detection case"),
    ],
)
def test_case_from_str_detection(enum, string, expected):
    assert_that(enum.from_str(string)).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, param, exception",
    [
        # Mandatory
        param(aD, None, MandatoryFieldError, id="All detection with None"),
        param(pD, None, MandatoryFieldError, id="Planet detection with None"),
        param(mD, None, MandatoryFieldError, id="Mass detection with None"),
        param(rD, None, MandatoryFieldError, id="Radius detection with None"),
        param(ddD, None, MandatoryFieldError, id="Detected disc detection with None"),
        # Not str
        param(aD, 1, TypeError, id="All detection with int"),
        param(pD, 1, TypeError, id="Planet detection with int"),
        param(mD, 1, TypeError, id="Mass detection with int"),
        param(rD, 1, TypeError, id="Radius detection with int"),
        param(ddD, 1, TypeError, id="Detected disc detection with int"),
        # Unknown
        param(aD, "toto", UnknownDetectionTypeError, id="All detection with unknown"),
        param(
            pD,
            "toto",
            UnknownDetectionTypeError,
            id="Planet detection with unknown",
        ),
        param(mD, "toto", UnknownDetectionTypeError, id="Mass detection with unknown"),
        param(
            rD,
            "toto",
            UnknownDetectionTypeError,
            id="Radius detection with unknown",
        ),
        param(
            ddD,
            "toto",
            UnknownDetectionTypeError,
            id="Detected disc detection with unknown",
        ),
    ],
)
def test_exception_from_str_detection(enum, param, exception):
    with pytest.raises(exception):
        enum.from_str(param)
