# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exochoices import PlanetStatus as pS
from exochoices import PublicationStatuses as pubS
from exochoices import WebStatus as wS
from exochoices.exceptions import MandatoryFieldError, UnknownStatusError


@pytest.mark.parametrize(
    "choice, expected_int, expected_str",
    [
        # Planet Statuses
        param(pS.CONFIRMED, 1, "Confirmed", id="confirmed; planet"),
        param(pS.CANDIDATE, 2, "candidate", id="candidate; planet"),
        param(pS.CONTROVERSIAL, 3, "Controversial", id="controversial; planet"),
        param(pS.UNCONFIRMED, 4, "Unconfirmed", id="unconfirmed; planet"),
        param(pS.RETRACTED, 5, "Retracted", id="retracted; planet"),
        # Web Statuses
        param(wS.ACTIVE, 1, "Active", id="active; web"),
        param(wS.HIDDEN, 3, "Hidden", id="hidden; web"),
        param(wS.IMPORTED, 4, "Imported", id="imported; web"),
        param(wS.SUGGESTED, 5, "Suggested", id="suggested; web"),
    ],
)
def test_planet_and_web_statuses(choice, expected_int, expected_str):
    with soft_assertions():
        assert_that(choice.as_int).is_equal_to(expected_int)
        assert_that(choice.as_str).is_equal_to(expected_str)


@pytest.mark.parametrize(
    "choice, expected_int, expected_str, expected_desc",
    [
        param(
            pubS.PUBLISHED,
            1,
            "R",
            "Published in a refereed paper",
            id="published in a paper",
        ),
        param(
            pubS.SUBMITTED,
            2,
            "S",
            "Submitted to a professional journal",
            id="submitted in a journal",
        ),
        param(
            pubS.ANNOUNCED_CONFERENCE,
            3,
            "C",
            "Announced on a professional conference",
            id="announced in a conference",
        ),
        param(
            pubS.ANNOUNCED_WEBSITE,
            4,
            "W",
            "Announced on a website",
            id="announced on web",
        ),
    ],
)
def test_publication_statuses(choice, expected_int, expected_str, expected_desc):
    with soft_assertions():
        assert_that(choice.as_int).is_equal_to(expected_int)
        assert_that(choice.as_str).is_equal_to(expected_str)
        assert_that(choice.desc).is_equal_to(expected_desc)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            pS,
            [
                "Confirmed",
                "candidate",
                "Controversial",
                "Unconfirmed",
                "Retracted",
            ],
            id="planet status",
        ),
        param(
            pubS,
            [
                "R",
                "S",
                "C",
                "W",
            ],
            id="publication status",
        ),
        param(
            wS,
            [
                "Active",
                "Hidden",
                "Imported",
                "Suggested",
            ],
            id="web status",
        ),
    ],
)
def test_as_str_list_status(enum, expected):
    assert_that(enum.as_str_list()).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, expected",
    [
        param(
            pS,
            [
                ("Confirmed", 1),
                ("candidate", 2),
                ("Controversial", 3),
                ("Unconfirmed", 4),
                ("Retracted", 5),
            ],
            id="planet status",
        ),
        param(
            pubS,
            [
                ("R", 1),
                ("S", 2),
                ("C", 3),
                ("W", 4),
            ],
            id="publication status",
        ),
        param(
            wS,
            [
                ("Active", 1),
                ("Hidden", 3),
                ("Imported", 4),
                ("Suggested", 5),
            ],
            id="web status",
        ),
    ],
)
def test_as_doc_status(enum, expected):
    assert_that(enum.as_doc()).is_equal_to(expected)


@pytest.mark.parametrize(
    "choice, expected",
    [
        # Planet Statuses
        (pS.CONFIRMED, 1),
        (pS.CANDIDATE, 2),
        (pS.CONTROVERSIAL, 3),
        (pS.UNCONFIRMED, 4),
        (pS.RETRACTED, 5),
        # Web Statuses
        (wS.ACTIVE, 1),
        (wS.HIDDEN, 3),
        (wS.IMPORTED, 4),
        (wS.SUGGESTED, 5),
        # Publication Statuses
        (pubS.PUBLISHED, 1),
        (pubS.SUBMITTED, 2),
        (pubS.ANNOUNCED_CONFERENCE, 3),
        (pubS.ANNOUNCED_WEBSITE, 4),
    ],
)
def test_as_decimal_status(choice, expected):
    assert_that(choice.as_decimal).is_equal_to(Decimal(expected))


@pytest.mark.parametrize(
    "enum, strings, expected",
    [
        param(pS, pS.as_str_list(), list(pS), id="Planet status"),
        param(wS, wS.as_str_list(), list(wS), id="Web status"),
        param(pubS, pubS.as_str_list(), list(pubS), id="Publication status"),
    ],
)
def test_from_str_status(enum, strings, expected):
    with soft_assertions():
        for string, expect in zip(strings, expected):
            assert_that(enum.from_str(string)).is_equal_to(expect)


@pytest.mark.parametrize(
    "enum, string, expected",
    [
        param(pS, "canDIdate", pS.CANDIDATE, id="Planet status case"),
        param(wS, "ACtiVE", wS.ACTIVE, id="Web status case"),
        param(pubS, "s", pubS.SUBMITTED, id="Publication status case"),
    ],
)
def test_case_from_str_status(enum, string, expected):
    assert_that(enum.from_str(string)).is_equal_to(expected)


@pytest.mark.parametrize(
    "enum, param, exception",
    [
        # Mandatory
        param(pS, None, MandatoryFieldError, id="Planet status with None"),
        param(wS, None, MandatoryFieldError, id="Web status with None"),
        param(pubS, None, MandatoryFieldError, id="Publication status with None"),
        # Not str
        param(pS, 1, TypeError, id="Planet status with int"),
        param(wS, 1, TypeError, id="Web status with int"),
        param(pubS, 1, TypeError, id="Publication status with int"),
        # Unknown
        param(pS, "toto", UnknownStatusError, id="Planet status with unknown"),
        param(wS, "toto", UnknownStatusError, id="Web status with unknown"),
        param(pubS, "toto", UnknownStatusError, id="Publication status with unknown"),
    ],
)
def test_exception_from_str_status(enum, param, exception):
    with pytest.raises(exception):
        enum.from_str(param)
