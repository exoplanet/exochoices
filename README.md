# Exo Choices

Little package for all useful Enum in exo-project.

All Enum are based on a namedtuple.

```python
Choice = namedtuple("Choice", ["as_int", "as_str"])
```

and for `PublicationStatuses`.

```python
ChoiceWithDesc = namedtuple("ChoiceWithDesc", ["as_int", "as_str", "desc"])
```

## Detection Enum

- PlanetDetection
- MassDetection
- RadiusDetection
- DetectedDiscDetection

## Object2pubparam Enum

- Plan2PubParam
- Star2PubParam

## Publication Enum

- PublicationType

## Status Enum

- PlanetStatus
- PublicationStatuses
- WebStatus

## Unit Enum

- MassUnit
- RadiusUnit

## Classes Methods

### as_str_list

You can output a list of str values with the `as_str_list` method.

```python
PlanetStatus.as_str_list()
# Result is: ["Confirmed", "candidate", "Controversial", "Unconfirmed", "Retracted"]
```

## Contacts

- Author: Ulysse CHOSSON (LESIA)
- Maintainer: Ulysse CHOSSON (LESIA)
- Email: <ulysse.chosson@obspm.fr>
- Contributors:
  - Pierre-Yves MARTIN (LESIA)
