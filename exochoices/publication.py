"""Different Publication Types used in exoplanet.eu database."""


# Local imports
from .choices import Choice, ChoiceEnum


class PublicationType(ChoiceEnum):
    """All possible publication support."""

    BOOK = Choice(as_int=1, as_str="book")
    THESIS = Choice(as_int=2, as_str="thesis")
    REFEREED_ARTICLE = Choice(as_int=3, as_str="refereed article")
    PROCEEDING = Choice(as_int=4, as_str="proceeding")
    UNKNOWN = Choice(as_int=5, as_str="unknown")
    REPORT = Choice(as_int=6, as_str="report")
    ARXIV = Choice(as_int=7, as_str="arXiv")
