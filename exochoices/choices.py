"""Choice classes for all Enumerable."""

# pylint: disable=duplicate-code

# Standard imports
from collections import namedtuple
from decimal import Decimal
from enum import Enum
from typing import List, Tuple

# Local imports
from .exceptions import MandatoryFieldError, get_error

Choice = namedtuple("Choice", ["as_int", "as_str"])

ChoiceWithDesc = namedtuple("ChoiceWithDesc", ["as_int", "as_str", "desc"])


class ChoiceEnum(Choice, Enum):
    """Base Enumerable for all Enum. Contains only class method(s)."""

    @classmethod
    def as_str_list(cls) -> List[str]:
        """Return a list of all as_str values."""
        return [value.as_str for value in list(cls)]  # type: ignore

    @classmethod
    def as_doc(cls) -> List[Tuple[str, int]]:
        """Return a list of all correspondence between as_str and as_int."""
        return [(value.as_str, value.as_int) for value in list(cls)]  # type: ignore

    @classmethod
    def from_str(cls, string: str) -> Choice | None:
        """
        Return the element the enum cls corresponding to the string.

        Args:
            string: string corresponding to an element of the enum cls.

        Returns:
            An element of the enum cls.

        Raises:
            MandatoryFieldError: Raised when string is None.
            TypeError: Raised is string isn't of str type.
            UnknwonError: Indirect raises by `get_error()`.
            UnknownPubParamError: Indirect raises by `get_error()`.
            UnknownPublicationTypeError: Indirect raises by `get_error()`.
            UnknownStatusError: Indirect raises by `get_error()`.
            UnknwonUnitError: Indirect raises by `get_error()`.
        """
        if not string:
            raise MandatoryFieldError(cls.__name__, cls.as_doc())

        if not isinstance(string, str):
            raise TypeError(f"Str is expected. All values: {cls.as_doc()}")

        for choice in cls:
            if string.lower() == choice.as_str.lower():
                return choice

        get_error(cls.__name__, string, cls.as_str_list())
        return None  # pragma: no cover

    @property
    def as_decimal(self) -> Decimal:
        """Return as_int value in decimal."""
        return Decimal(self.as_int)
