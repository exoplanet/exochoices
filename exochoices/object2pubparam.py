"""Different Links for publication used in exoplanet.eu database."""

# Local imports
from .choices import Choice, ChoiceEnum


class Star2PubParam(ChoiceEnum):
    """
    All star parameters that can be linked to a publication and a generic one.

    It's useful to tell if a publication is about a specific star parameter
    or more generic one.
    """

    GENERIC_PUB = Choice(as_int=0, as_str="<generic_pub>")
    DISTANCE = Choice(as_int=1, as_str="distance")
    MASS = Choice(as_int=2, as_str="mass")
    AGE = Choice(as_int=3, as_str="age")
    TEFF = Choice(as_int=4, as_str="teff")
    RADIUS = Choice(as_int=5, as_str="radius")
    METALLICITY = Choice(as_int=6, as_str="metallicity")
    MAGNETIC_FIELD = Choice(as_int=7, as_str="magnetic_field")
    DETECTED_DISC = Choice(as_int=8, as_str="detected_disc")


class Plan2PubParam(ChoiceEnum):
    """
    All planet parameters that can be linked to a publication and a generic one.

    It's useful to tell if a publication is about a specific planet parameter
    or more generic one.
    """

    GENERIC_PUB = Choice(as_int=0, as_str="<generic_pub>")
    MASS = Choice(as_int=1, as_str="mass")
    RADIUS = Choice(as_int=2, as_str="radius")
    AXIS = Choice(as_int=3, as_str="axis")
    PERIOD = Choice(as_int=4, as_str="period")
    ECCENTRICITY = Choice(as_int=5, as_str="eccentricity")
    OMEGA = Choice(as_int=6, as_str="omega")
    INCLINATION = Choice(as_int=7, as_str="inclination")
    TZERO_TR = Choice(as_int=8, as_str="tzero_tr")
    TZERO_VR = Choice(as_int=9, as_str="tzero_vr")
    TPERI = Choice(as_int=10, as_str="tperi")
    TCONJ = Choice(as_int=11, as_str="tconj")
    TEMP_CALCULATED = Choice(as_int=12, as_str="temp_calculated")
    TEMP_MEASURED = Choice(as_int=13, as_str="temp_measured")
    HOT_POINT_LON = Choice(as_int=14, as_str="hot_point_lon")
    LOG_G = Choice(as_int=15, as_str="log_g")
    TZERO_TR_SEC = Choice(as_int=16, as_str="tzero_tr_sec")
    LAMBDA_ANGLE = Choice(as_int=17, as_str="lambda_angle")
    K = Choice(as_int=18, as_str="k")
    GEOMETRIC_ALBEDO = Choice(as_int=19, as_str="geometric_albedo")
    IMPACT_PARAMETER = Choice(as_int=20, as_str="impact_parameter")
    MASS_SINI = Choice(as_int=21, as_str="mass_sini")
