"""Different Statuses used in exoplanet.eu database."""

# pylint: disable=duplicate-code

# Standard imports
from decimal import Decimal
from enum import Enum
from typing import List, Tuple

# Local imports
from .choices import Choice, ChoiceEnum, ChoiceWithDesc
from .exceptions import MandatoryFieldError, get_error


class PlanetStatus(ChoiceEnum):
    """All possible planet statuses."""

    CONFIRMED = Choice(as_int=1, as_str="Confirmed")
    # BUG: change this to have a capital letter
    CANDIDATE = Choice(as_int=2, as_str="candidate")
    CONTROVERSIAL = Choice(as_int=3, as_str="Controversial")
    UNCONFIRMED = Choice(as_int=4, as_str="Unconfirmed")
    RETRACTED = Choice(as_int=5, as_str="Retracted")


class WebStatus(ChoiceEnum):
    """All possible statuses for the visibility of a planet on the website."""

    ACTIVE = Choice(as_int=1, as_str="Active")
    HIDDEN = Choice(as_int=3, as_str="Hidden")
    IMPORTED = Choice(as_int=4, as_str="Imported")
    SUGGESTED = Choice(as_int=5, as_str="Suggested")


class PublicationStatuses(ChoiceWithDesc, Enum):
    """All possible publication statuses."""

    PUBLISHED = ChoiceWithDesc(
        as_int=1,
        as_str="R",
        desc="Published in a refereed paper",
    )
    SUBMITTED = ChoiceWithDesc(
        as_int=2,
        as_str="S",
        desc="Submitted to a professional journal",
    )
    ANNOUNCED_CONFERENCE = ChoiceWithDesc(
        as_int=3,
        as_str="C",
        desc="Announced on a professional conference",
    )
    ANNOUNCED_WEBSITE = ChoiceWithDesc(
        as_int=4,
        as_str="W",
        desc="Announced on a website",
    )

    @classmethod
    def as_str_list(cls) -> List[str]:
        """Return a list of all as_str values."""
        return [value.as_str for value in list(cls)]  # type: ignore

    @classmethod
    def as_doc(cls) -> List[Tuple[str, int]]:
        """Return a list of all correspondence between as_str and as_int."""
        return [(value.as_str, value.as_int) for value in list(cls)]  # type: ignore

    @classmethod
    def from_str(cls, string: str) -> ChoiceWithDesc | None:
        """
        Return the element the enum cls corresponding to the string.

        Args:
            string: string corresponding to an element of the enum cls.

        Returns:
            An element of the enum cls.

        Raises:
            MandatoryFieldError: Raised when string is None.
            TypeError: Raised is string isn't of str type.
            UnknwonError: Indirect raises by `get_error()`.
            UnknownDetectionTypeError: Indirect raises by `get_error()`.
        """
        if not string:
            raise MandatoryFieldError(cls.__name__, cls.as_doc())

        if not isinstance(string, str):
            raise TypeError(f"Str is expected. All values: {cls.as_doc()}")

        for choice in cls:
            if string.lower() == choice.as_str.lower():
                return choice

        get_error(cls.__name__, string, cls.as_str_list())
        return None  # pragma: no cover

    @property
    def as_decimal(self) -> Decimal:
        """Return as_int value in decimal."""
        return Decimal(self.as_int)
