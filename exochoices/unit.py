"""Different Units used in exoplanet.eu database."""

# Local imports
from .choices import Choice, ChoiceEnum


class MassUnit(ChoiceEnum):
    """All possible units of mass."""

    MJUP = Choice(as_int=1, as_str="Mjup")
    MEARTH = Choice(as_int=2, as_str="Mearth")


class RadiusUnit(ChoiceEnum):
    """All possible units of radius."""

    RJUP = Choice(as_int=1, as_str="Rjup")
    REARTH = Choice(as_int=2, as_str="Rearth")
