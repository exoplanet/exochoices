"""Different Detection Types used in exoplanet.eu database."""


# Local imports
from .choices import Choice, ChoiceEnum


class _AllDetection(ChoiceEnum):
    """
    All possible detection method.

    This class must never be used directly. It is only to ensure coerence between the
    different specialised *Detection classes* that reuse its values.
    """

    RADIAL_VELOCITY = Choice(as_int=1, as_str="Radial Velocity")
    TIMING = Choice(as_int=2, as_str="Timing")
    CONTROVERSIAL = Choice(as_int=3, as_str="Controversial")
    MICROLENSING = Choice(as_int=4, as_str="Microlensing")
    IMAGING = Choice(as_int=5, as_str="Imaging")
    PRIMARY_TRANSIT = Choice(as_int=6, as_str="Primary Transit")
    ASTROMETRY = Choice(as_int=7, as_str="Astrometry")
    TTV = Choice(as_int=8, as_str="TTV")
    OTHER = Choice(as_int=9, as_str="Other")
    SPECTRUM = Choice(as_int=10, as_str="Spectrum")
    THEORETICAL = Choice(as_int=11, as_str="Theoretical")
    FLUX = Choice(as_int=12, as_str="Flux")
    SECONDARY_TRANSIT = Choice(as_int=13, as_str="Secondary Transit")
    IR_EXCESS = Choice(as_int=14, as_str="IR Excess")


class PlanetDetection(ChoiceEnum):
    """All detection method applicable to planets."""

    RADIAL_VELOCITY = _AllDetection.RADIAL_VELOCITY
    TIMING = _AllDetection.TIMING
    MICROLENSING = _AllDetection.MICROLENSING
    IMAGING = _AllDetection.IMAGING
    PRIMARY_TRANSIT = _AllDetection.PRIMARY_TRANSIT
    ASTROMETRY = _AllDetection.ASTROMETRY
    TTV = _AllDetection.TTV
    OTHER = _AllDetection.OTHER
    SECONDARY_TRANSIT = _AllDetection.SECONDARY_TRANSIT


class MassDetection(ChoiceEnum):
    """All detection method applicable to mass detection."""

    RADIAL_VELOCITY = _AllDetection.RADIAL_VELOCITY
    TIMING = _AllDetection.TIMING
    CONTROVERSIAL = _AllDetection.CONTROVERSIAL
    MICROLENSING = _AllDetection.MICROLENSING
    ASTROMETRY = _AllDetection.ASTROMETRY
    TTV = _AllDetection.TTV
    SPECTRUM = _AllDetection.SPECTRUM
    THEORETICAL = _AllDetection.THEORETICAL


class RadiusDetection(ChoiceEnum):
    """All detection method applicable to radius detection."""

    PRIMARY_TRANSIT = _AllDetection.PRIMARY_TRANSIT
    THEORETICAL = _AllDetection.THEORETICAL
    FLUX = _AllDetection.FLUX


class DetectedDiscDetection(ChoiceEnum):
    """All detection method applicable to detected disc."""

    IMAGING = _AllDetection.IMAGING
    IR_EXCESS = _AllDetection.IR_EXCESS
