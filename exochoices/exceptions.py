"""Error of the project."""

# Standard imports
from typing import List, Tuple, cast


class UnknownError(Exception):
    """Master exception for Unknown___Error."""

    def __init__(
        self,
        rec: str,
        expec: List[str] | List[Tuple[str, int]],
        error_type: str,
    ):
        """Initialize an Unknown___Error."""
        super().__init__(f"Unknown {error_type}: {rec}. Expected {error_type}: {expec}")


class UnknownStatusError(UnknownError):
    """Exception raised when a status in unknown."""

    def __init__(self, receive: str, expected: List[str]):
        """Initialize an UnknownStatusError."""
        super().__init__(receive, expected, "status")


class UnknownUnitError(UnknownError):
    """Exception raised when a unit in unknown."""

    def __init__(self, receive: str, expected: List[str]):
        """Initialize an UnknownUnitError."""
        super().__init__(receive, expected, "unit")


class UnknownDetectionTypeError(UnknownError):
    """Exception raised when a detection type in unknown."""

    def __init__(self, receive: str, expected: List[str]):
        """Initialize an UnknownDetectionTypeError."""
        super().__init__(receive, expected, "detection type")


class UnknownPublicationTypeError(UnknownError):
    """Exception raised when a publication type in unknown."""

    def __init__(self, receive: str, expected: List[str]):
        """Initialize an UnknownPublicationTypeError."""
        super().__init__(receive, expected, "publication type")


class UnknownPubParamError(UnknownError):
    """Exception raised when a publication type in unknown."""

    def __init__(self, receive: str, expected: List[str]):
        """Initialize an UnknownPublicationTypeError."""
        super().__init__(receive, expected, "publication parameter")


class MandatoryFieldError(Exception):
    """Exception raised when a mandatory field is none."""

    def __init__(self, name: str, expected: List[Tuple[str, int]]):
        """Initialize a MandatoryFieldError."""
        super().__init__(f"The field {name} is mandatory. Expected values: {expected}")


def get_error(
    enum_name: str,
    receive: str,
    expected: List[str] | List[Tuple[str, int]],
) -> None:
    """
    Get the correct error corresponding of a name of a Enum.

    Args:
        enum_name: Name of the enum.
        receive: Value receive by `from_str()`.
        expected: Expected values.

    Raises:
        UnknownDetectionTypeError: Raised when `enum_name` is in [
                PlanetDetection,
                MassDetection,
                RadiusDetection,
                DetectedDiscDetection,
                _AllDetection,
            ]
        UnknownPubParamError: Raised when `enum_name` is in [
                Plan2PubParam,
                Star2PubParam
            ]
        UnknownPublicationTypeError: Raised when `enum_name` is in [PublicationType]
        UnknownStatusError: Raised when `enum_name` is in [
                PlanetStatus,
                WebStatus,
                PublicationStatuses
            ]
        UnknwonUnitError: Raised when `enum_name` is in [MassUnit, RadiusUnit]
        UnknwonError: Raised when `enum_name` is unknown.
    """
    match enum_name:
        case str() if enum_name in [
            "PlanetDetection",
            "MassDetection",
            "RadiusDetection",
            "DetectedDiscDetection",
            "_AllDetection",
        ]:
            raise UnknownDetectionTypeError(receive, cast(List[str], expected))
        case str() if enum_name in ["Plan2PubParam", "Star2PubParam"]:
            raise UnknownPubParamError(receive, cast(List[str], expected))
        case str() if enum_name in ["PublicationType"]:
            raise UnknownPublicationTypeError(receive, cast(List[str], expected))
        case str() if enum_name in ["PlanetStatus", "WebStatus", "PublicationStatuses"]:
            raise UnknownStatusError(receive, cast(List[str], expected))
        case str() if enum_name in ["MassUnit", "RadiusUnit"]:
            raise UnknownUnitError(receive, cast(List[str], expected))
        case _:
            raise UnknownError(receive, expected, "Unknown")
