# Changelog

<!-- markdownlint-disable-file MD024 -->

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2022-12-01

### Added

- Add a property for `PublicationStatus`: `as_decimal` to get a int value in decimal.
- Add a property for `ChoiceEnum`: `as_decimal` to get a int value in decimal.
- Add a classmethod for `PublicationStatus`: `from_str` to get a element of Enum
    corresponding to a string.
- Add a classmethod for `ChoiceEnum`: `from_str` to get a element of Enum
    corresponding to a string.

## [1.2.1] - 2022-11-30

### Added

- Add a classmethod for `PublicationStatus`: `as_doc` to get all str and int values.
- Add a classmethod for `ChoiceEnum`: `as_doc` to get all str and int values.

### Fixed

- Fix a typo in publication; `preceeding` to `proceeding`.

## [1.1.0] - 2022-11-24

### Added

- Add a classmethod for `PublicationStatus`: `as_str_list` to get all str value in a list.
- Add a classmethod for `ChoiceEnum`: `as_str_list` to get all str value in a list.
- Add `ChoiseEnum` as parent class of others classes except `PublicationStatus`.

## [1.0.0] - 2022-11-23

### Added

- Add AllDetection Enum.
- Add PlanetDetection Enum.
- Add MassDetection Enum.
- Add RadiusDetection Enum.
- Add DetectedDiscDetection Enum.
- Add PlanetStatus Enum.
- Add WebStatus Enum.
- Add PublicationStatus Enum.
- Add PublicationType Enum.
- Add Star2PubParam Enum.
- Add Plan2PubParam Enum.
- Add MassUnit Enum.
- Add RadiusUnit Enum.
