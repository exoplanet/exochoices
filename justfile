# Remove __pycache__ and .pyc files and folders
clean:
    find . \( -name __pycache__ -o -name "*.pyc" \) -delete

# Clear and display pwd
clear :
    clear
    pwd

# Install no-dev dependencies with poetry
install: clean clear
    poetry install --no-dev --remove-untracked

# install all dependencies with poetry and npm
install-all: clean clear
    poetry install --remove-untracked
    npm install
    sudo npm install markdownlint-cli2@0.4.0 --global

# Launch exodam pytest
test : clear
    python3 -m pytest

# Launch test and clean command
pytest : test clean

# Update pre-commit hook and make a clean install of pre-commit dependencies
preupdate: clean clear
    pre-commit clean
    pre-commit autoupdate
    preinstall

# Do a clean install of pre-commit dependencies
preinstall: clean clear
    pre-commit install --hook-type pre-merge-commit
    pre-commit install --hook-type pre-push
    pre-commit install --hook-type post-rewrite
    pre-commit install-hooks
    pre-commit install

# Simulate a pre-commit check on added files
prepre: clean onmy31 clear
    #!/usr/bin/env sh
    set -eux pipefail
    git status
    pre-commit run --all-files

# Launch docstring verification with pydocstyle
pydocstyle path="exochoices": clean clear
    pydocstyle {{ path }}

# Launch the mypy type linter on the module
mypy: clean clear
    mypy --pretty -p exochoices --config-file pyproject.toml

# Run pylint
pylint path="exochoices": clean clear
    pylint --output-format=colorized --msg-template='{msg_id}: in the file: {path}, at line: {line:}, at column: {column}, in objects: {obj} -> {msg}' {{ path }}

# Run flakehell
flake8 path="exochoices": clean clear
    flake8 {{ path }}

# Run markdownlint
lintmd path='"**/*.md" "#node_modules"': clean clear
    markdownlint-cli2-config ".markdownlint-cli2.yaml" {{ path }}

# Run all linter
lint : clean clear onmy31 pydocstyle mypy pylint flake8 lintmd
    echo "\n\t\033[0;32mALL CLEAR\033[0m"

# Run black and isort
onmy31 path ="exochoices tests": clean clear
    black {{path}}
    isort {{path}}

# auto interactive rebase
autorebase: clean clear
    git rebase -i $(git merge-base $(git branch --show-current) main)

# rebase on main
rebaseM: clean clear
    git checkout main
    git pull
    git checkout -
    git rebase main

# Launch coverage on all
coverage: clean clear
    coverage run -m pytest
    clear
    coverage report -m --skip-covered --precision=3

# List all just commands
list:
    just --list
